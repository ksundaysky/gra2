package sample;

public class Dimensions
{

    final public int WIDTH = 1280;
    final public int HEIGHT = 720;

    final public int GOAL_GATE_WIDTH = WIDTH/20;
    final public int GOAL_GATE_HEIGHT = HEIGHT/4;

    double velocity=1/10;
    double delay=0.05;

    public static int scoreL=0 ;
    public static int scoreR =0;

    public void incrementL(){
        this.scoreL+=1;
    }

    public void incrementR(){
        this.scoreR+=1;
    }
}
