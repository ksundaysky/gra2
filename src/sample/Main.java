package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.Random;

import static java.awt.image.ImageObserver.HEIGHT;
import static java.awt.image.ImageObserver.WIDTH;
import static java.lang.Math.*;
import sample.Dimensions;

public class Main extends Application
{
    Dimensions dimension = new Dimensions();








    @FXML
    public Pane stackPane;

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        stackPane = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setScene(new Scene(stackPane, dimension.WIDTH, dimension.HEIGHT));
        primaryStage.setTitle("AI_Soccer");

        Ball kurde_ball= new Ball(dimension.WIDTH/2, dimension.HEIGHT/2,15,Color.BLUE);
        //DODAJEMY SWIATLO NA PILKE, CO BY BYLA PRAWDZIWSZA.  wyszedł gówniany efekt. ale mysle, że potencjal jest :D
        kurde_ball.getLight();

        Player player= new Player(0, 0,30,Color.rgb(224,65,2));

        stackPane.setOnMouseMoved(e->
        {
            player.playerX=e.getX();
            player.playerY=e.getY();
        });



        //DODAJEMY TABLICE WYNIKOWA.
        ScoreTable wynik = new ScoreTable();


        //Wrzucamy na PANE.
        stackPane.getChildren().addAll(wynik,kurde_ball,player);
        primaryStage.show();

        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(6),e->run(kurde_ball,player,wynik)));
        timeline.setCycleCount(timeline.INDEFINITE);
        timeline.play();
    }

    private void run(Ball ball, Player player,ScoreTable wynik)
    {
        double odl=sqrt(pow((ball.getLayoutX()-player.getLayoutX()),2)+pow((ball.getLayoutY()-player.getLayoutY()),2));

        ball.moveBall(odl,player);
        player.movePlayer();
        wynik.setView();

    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
