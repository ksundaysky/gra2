package sample;


import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import sample.Dimensions;
import java.util.Random;


public class Ball extends Circle{
    Dimensions dimension = new Dimensions();
    double posX;
    double posY;

    public Ball() { }

    public Ball(double x, double y, double r, Color color) {

        this.setLayoutX(x);
        this.setLayoutY(y);
        this.setRadius(r);
        this.setFill(color);
        this.posX=0;
        this.posY=0;
    }


    public void getLight()
    {
        Light.Distant light = new Light.Distant() ;
        light.setAzimuth(90.0) ;
        light.setElevation(10.0) ;
        Lighting lighting = new Lighting() ;
        this.setEffect(lighting);
    }

    public int isGoal( int counter){

        if(this.getLayoutY()>dimension.HEIGHT/2-dimension.GOAL_GATE_HEIGHT/2&&this.getLayoutY()<dimension.HEIGHT/2+dimension.GOAL_GATE_HEIGHT/2 )
        {
            System.out.println("GOOOOOL LEWA");
            counter++;
            this.setLayoutX(dimension.WIDTH/2);
            this.setLayoutY(dimension.HEIGHT/2);

        }

        return counter;


    }
    public void moveBall(double odl,Player player)
    {

        this.setLayoutX(this.getLayoutX() + this.posX);
        this.setLayoutY(this.getLayoutY() + this.posY);

        if((odl>player.getRadius()+this.getRadius()))
        {


            if (this.getLayoutY() <= dimension.GOAL_GATE_WIDTH / 2 + this.getRadius()) {
                this.posX = new Random().nextInt(5) - 2;
                this.posY = new Random().nextInt(3);
            } else if (this.getLayoutY() >= dimension.HEIGHT - dimension.GOAL_GATE_WIDTH / 2 - this.getRadius()) {
                this.posX = new Random().nextInt(5) - 2;
                this.posY = new Random().nextInt(3) - 2;

            } else if (this.getLayoutX() <= dimension.GOAL_GATE_WIDTH / 2 + this.getRadius()) {

                dimension.scoreL=isGoal(dimension.scoreL);
                this.posX = new Random().nextInt(3);
                this.posY = new Random().nextInt(5) - 2;

            } else if (this.getLayoutX() >= dimension.WIDTH - dimension.GOAL_GATE_WIDTH / 2 - this.getRadius()) {

                dimension.scoreR=isGoal(dimension.scoreR);
                this.posX = new Random().nextInt(3) - 2;
                this.posY = new Random().nextInt(5) - 2;
            }

            if (this.posX != 0)
            {
                if (this.posX > 0)
                    this.posX -= dimension.delay;
                else
                    this.posX += dimension.delay;
            }
            if (this.posY != 0)
            {
                if (this.posY > 0)
                    this.posY -= dimension.delay;
                else
                    this.posY += dimension.delay;
            }
        }
        else
        {
            dimension.velocity = 10;
            this.posX=(this.getLayoutX()-player.getLayoutX())/100*dimension.velocity; // wektor przesuniecia X, niezej Y
            System.out.println("X: " + this.posX);
            this.posY=(this.getLayoutY()-player.getLayoutY())/100*dimension.velocity;
            System.out.println("Y: " + this.posY+"\n");
        }
        this.setLayoutX(this.getLayoutX() + this.posX);
        this.setLayoutY(this.getLayoutY() + this.posY);
    }
}