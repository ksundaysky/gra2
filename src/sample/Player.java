package sample;

import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

public class Player extends Circle{

    double playerX;
    double playerY;

    public Player() {
    }

    public Player(double centerX, double centerY, double radius, Paint fill) {
        super(centerX, centerY, radius, fill);
        this.playerX=centerX;
        this.playerY=centerY;
    }

    public void movePlayer()
    {

        this.setLayoutX(this.playerX);
        this.setLayoutY(this.playerY);
    }

}
