package sample;

import javafx.scene.text.Font;
import javafx.scene.text.Text;
import sample.Dimensions;
public class ScoreTable extends Text{
    Dimensions dimension = new Dimensions();

    public ScoreTable() {
        this.setX(0);
        this.setY(0);
    }



    public ScoreTable(double x, double y, String text) {
        super(x, y, text);


    }

    public void setView() {
        this.setStrokeWidth(-200);
        this.setStyle("-fx-background-color: black");
        this.setFont(Font.font(100));
        this.setX(dimension.WIDTH / 2 - 100);
        this.setY(dimension.HEIGHT / 3);
        this.setText(dimension.scoreL+" : "+dimension.scoreR);
    }
}
